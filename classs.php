<?php


       class MyClass{

            public $thisIsProperty;


           /**
            * @return mixed
            */
           public function getThisIsProperty()
           {
               return $this->thisIsProperty;
           }


           /**
            * @param mixed $thisIsProperty
            */
           public function setThisIsProperty($thisIsProperty)
           {
               $this->thisIsProperty = $thisIsProperty;
           }

       }
